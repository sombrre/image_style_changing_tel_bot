import torch
import torchvision.transforms as transforms
from torch import nn
from PIL import Image
import asyncio

VGG_net = ''

class GeneratedImage(nn.Module):
    def __init__(self, img_shape, **kwargs):
        super(GeneratedImage, self).__init__(**kwargs)
        self.weight = nn.Parameter(torch.rand(*img_shape))

    def forward(self):
        return self.weight


class StyleTransfer:
    def __init__(self, chat_id, content_img, style_img, num_epochs=50,
                    style_weight=1e4, content_weight=0.1, tv_weight=1):

        self.device = "cpu"

        self.image_shape = (300, 450)
        self.rgb_mean = torch.tensor([0.485, 0.456, 0.406])
        self.rgb_std = torch.tensor([0.229, 0.224, 0.225])
        self.chat_id = chat_id
        self.num_epochs = num_epochs 
        self.style_weight = style_weight
        self.content_weight = content_weight
        self.tv_weight = tv_weight
        
        self.content_img = self.image_loader(content_img)
        self.input_img = self.content_img.clone()
        self.style_img = self.image_loader(style_img)
        
        self.content_layers = [25]
        self.style_layers = [0, 5, 10, 19, 28]

    def extract_features(self, X, content_layers, style_layers):
        contents = []
        styles = []
        for i in range(len(net)):
            X = net[i](X)
            if i in style_layers:
                styles.append(X)
            if i in content_layers:
                contents.append(X)
        return contents, styles

   
    def image_loader(self, image_name):
        transform_loader = transforms.Compose([
        transforms.Resize(self.image_shape),
        transforms.ToTensor(),
        transforms.Normalize(mean=self.rgb_mean, std=self.rgb_std)])
        image = Image.open(image_name)
        image = transform_loader(image).unsqueeze(0)
        return image
    
    def gram(self, X):
        num_channels, n = X.shape[1], X.numel() // X.shape[1]
        X = X.reshape((num_channels, n))
        return torch.matmul(X, X.T) / (num_channels * n)
    
    def compute_loss(self, X, contents_Y_hat, styles_Y_hat, contents_Y, styles_Y_gram):
        # Calculate the content, style, and total variance losses respectively
        contents_l = [torch.square(Y_hat - Y.detach()).mean() * self.content_weight for Y_hat, Y in zip(
            contents_Y_hat, contents_Y)]
        styles_l = [(torch.square(self.gram(Y_hat) - Y.detach()).mean()) * self.style_weight for Y_hat, Y in zip( 
            styles_Y_hat, styles_Y_gram)]
        
        tv_loss = 0.5 * (torch.abs(X[:, :, 1:, :] - X[:, :, :-1, :]).mean() +
                    torch.abs(X[:, :, :, 1:] - X[:, :, :, :-1]).mean())
        tv_l = tv_loss * self.tv_weight
        # Add up all the losses
        l = sum(styles_l + contents_l + [tv_l])
        return contents_l, styles_l, tv_l, l
    
    def get_inits(self, X, lr, styles_Y):
        gen_img = GeneratedImage(X.shape).to(self.device)
        gen_img.weight.data.copy_(X.data)
        trainer = torch.optim.Adam(gen_img.parameters(), lr=lr)
        styles_Y_gram = [self.gram(Y) for Y in styles_Y]
        return gen_img(), styles_Y_gram, trainer
    
    async def train(self, X, contents_Y, styles_Y, lr, lr_decay_epoch):
        history = []
        
        X, styles_Y_gram, trainer = self.get_inits(X, lr, styles_Y)
        scheduler = torch.optim.lr_scheduler.StepLR(trainer, lr_decay_epoch, 0.8)
        for epoch in range(self.num_epochs):
            await asyncio.sleep(0)
            if epoch%10 == 0:
                print(f'epoch {epoch} for {self.chat_id}')
            trainer.zero_grad()
            contents_Y_hat, styles_Y_hat = self.extract_features(
                X, self.content_layers, self.style_layers)
            await asyncio.sleep(0)
            contents_l, styles_l, tv_l, l = self.compute_loss(X, 
                        contents_Y_hat, styles_Y_hat, contents_Y, styles_Y_gram)
            l.backward()
            trainer.step()
            scheduler.step()
            history.append((float(sum(contents_l)), float(sum(styles_l)), float(tv_l)))

        return X, history

    def postprocessed(self, img):
        img = img[0].to(self.rgb_std.device)
        img = torch.clamp(img.permute(1, 2, 0) * self.rgb_std + self.rgb_mean, 0, 1)
        return transforms.ToPILImage()(img.permute(2, 0, 1))


    async def transfer(self):
        global VGG_net
        global net
        if VGG_net == '':
            VGG_net = torch.load(r'models\full_vgg19_pr.pth',).to(self.device).eval() 
        net = nn.Sequential(*[VGG_net.features[i] for i in
                      range(max(self.content_layers + self.style_layers) + 1)])
        net = net.to(self.device)
        content_X = self.content_img.to(self.device)
        contents_Y, _ = self.extract_features(content_X, self.content_layers, self.style_layers)
        style_X = self.style_img.to(self.device)
        _, styles_Y = self.extract_features(style_X, self.content_layers, self.style_layers)
        
        output, history = await self.train(content_X, contents_Y, styles_Y, 0.1, 5)

        return self.postprocessed(output)
