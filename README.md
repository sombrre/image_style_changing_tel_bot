Image_style_changing_tel_bot
==============================

DLS course project "Styling images in a telegram bot".
This telegram bot makes simple transfer style from one image to another (NST algorythm).
Algorithm runs in CPU with Pytorch library.

Project Organization
------------
   
    ├── LICENSE
    
    ├── README.md          <- The top-level README for developers using this project.
    ├── bot.py             <- Script with source code for telegram bot in this project.
    ├── data
    │   ├── external       <- Data from third party sources.
    │   ├── interim        <- Intermediate data that has been transformed.
    │   ├── processed      <- The final, canonical data sets for modeling.
    │   └── raw            <- The original, immutable data dump.
    │
    ├── models             <- Trained model
    │
    ├── notebooks          <- Jupyter notebooks with Neural Style Transfer
    │
    ├── requirements.txt   <- The requirements file for reproducing the analysis environment, e.g.
    │                         generated with `pip freeze > requirements.txt`
    │
    └── style_transfer.py   <- Script to turn input image into style-modeling image


--------
You can to add the bot_run.bat file near the main directory to run this bot. The file must contain:

@echo off    <It will disable the output of commands to the screen during the execution of the batch file>

call %~dp0<you path to virtual env activate>

cd %~dp0<you path to bot_run.bat>

set TOKEN=<your bot token, which can be obtained from the official bot of the Telegram service to create your own bots: @BotFather>

python bot.py

pause

