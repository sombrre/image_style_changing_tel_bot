# async style transfer
from io import BytesIO

from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor
from aiogram.types import InlineKeyboardMarkup
from aiogram.types import InlineKeyboardButton
from aiogram.types import ContentType, Message

import logging
from copy import deepcopy
import os

from style_transfer import *

logging.basicConfig(level=logging.INFO)
bot = Bot(os.getenv('TOKEN'))
dispatcher = Dispatcher(bot)

image_buffer = {}

cancel_kb = InlineKeyboardMarkup()
cancel_kb.add( InlineKeyboardButton('Отмена', callback_data='main_menu'))

# beginning
@dispatcher.message_handler(commands=['start'])
async def send_welcome(message):
    await bot.send_message(message.chat.id,
        f"Привет, {message.from_user.first_name}!\nЯ Style transfer бот. " +
        "Я умею переносить стиль с одной фотографии на другую. " +
        "Пришли мне изображение, на которое нужно перенести стиль")
    image_buffer[message.chat.id] = InfoAboutUser()    


@dispatcher.message_handler(commands=['help'])
async def process_help_command(message: types.Message):
    await message.reply("Я Style transfer бот. " +
        "Я умею переносить стиль с одного изображения на другое."+
        "Пришли мне два изображения в формате .jpg/.jpeg, сначала основное изображение, " + 
        "а затем изображение, с которого нужно перенести стиль на первое.")
    image_buffer[message.chat.id] = InfoAboutUser() 

    
@dispatcher.message_handler()
async def echo_message(message):
    if 'привет' in message.text.lower() or 'hello' in message.text.lower():
       await message.answer(f"Привет, {message.from_user.first_name}!\nЯ Style transfer бот. " +
        "Я умею переносить стиль с одной фотографии на другую. " +
        "Пришли мне изображение, на которое нужно перенести стиль")
    elif 'да' in message.text.lower():
       await message.answer(f"Отлично!" +
        "\nПришли мне изображение, на которое нужно перенести стиль")
    else:
        await message.answer('Я не понимаю тебя.' + 
                        'Нажми /help для получения справки, или напиши "привет" и я расскажу, что я умею')
    
    image_buffer[message.chat.id] = InfoAboutUser() 

# getting image
@dispatcher.message_handler(content_types=ContentType.PHOTO)
async def send_photo_file_id(message: Message):
    if message.content_type == 'photo':
        img_id = message.photo[-1].file_id
    file_info = await bot.get_file(img_id)
    photo = await bot.download_file(file_info.file_path)

    image_buffer[message.chat.id].photos.append(photo)

    # style transfer
    if len(image_buffer[message.chat.id].photos) == 1:
        await bot.send_message(message.chat.id,
                "Отлично, теперь пришли мне изображение стиля, чтобы я мог наложить его на изображение выше",)

    elif len(image_buffer[message.chat.id].photos) == 2:
        await bot.send_message(message.chat.id, "Начинаю обрабатывать, это займет примерно 5 минут...")
        data = deepcopy(image_buffer[message.chat.id].photos)
        try:
            output = await style_transfer(StyleTransfer, message.from_user.username, *data)
            await bot.send_message(message.chat.id, "Результат обработки:")
            await bot.send_photo(message.chat.id, deepcopy(output))
            await bot.send_message(message.chat.id, 'Если хочешь обработать еще несколько изображений, напиши "Да"')
        except:
            await bot.send_message(message.chat.id, "Произошла ошибка. Попробуй уменьшить размер фото или " +
                                   "напиши /help для получения справки.")
        
        del image_buffer[message.chat.id].photos

class InfoAboutUser:
    def __init__(self):
        self.settings = {'num_epochs': 50}
        self.photos = []
        self.username = []

    def set_default_settings(self):
        self.settings = {'num_epochs': 50}
        
async def style_transfer(st_class, *imgs):
    st = st_class(*imgs)
    output = await st.transfer()
    output_ = img_to_BytesIO(output)
    return output_

def img_to_BytesIO(output):
    bio = BytesIO()
    bio.name = 'result.jpeg'
    output.save(bio, 'JPEG')
    bio.seek(0)
    return bio

if __name__ == '__main__':
    executor.start_polling(dispatcher)
